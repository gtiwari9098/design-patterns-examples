# design-patterns-examples

Working example of all design patterns
Creational Design pattern
    * Singleton 
*         Eager Initialisation
*         Static Initialisation
*         Lazy Initialisation 
*         Double Checked Locking / Threadsafe intialisation using volatile
*         Enum Singleton
*         Destroy singlton using reflection
*     Factory design pattern
*     Abstract design pattern
*     Builder design pattern
*     Prototype design pattern


Structural design pattern
Adapter pattern
*     composite pattern
*     proxy pattern
*     flyweight pattern
*     facade pattern
*     Bridge pattern
*     Decorator pattern


Behvaioural Design pattern


