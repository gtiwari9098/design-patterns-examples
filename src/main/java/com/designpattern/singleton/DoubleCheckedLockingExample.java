package com.designpattern.singleton;

public class DoubleCheckedLockingExample {
    public static volatile DoubleCheckedLockingExample singletonInstance;

    static DoubleCheckedLockingExample getSingletonInstance () {
        if(null == singletonInstance){
            synchronized (DoubleCheckedLockingExample.class) {
                if(null == singletonInstance) {
                    singletonInstance = new DoubleCheckedLockingExample();
                }
            }
        }
        return  singletonInstance;
    }
    protected Object readResolve() {
        return getSingletonInstance();
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }
}