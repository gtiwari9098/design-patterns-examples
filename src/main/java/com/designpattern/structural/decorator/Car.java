package com.designpattern.structural.decorator;

public interface Car {

	public void assemble();
}