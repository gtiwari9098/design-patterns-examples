package com.designpattern.factory;

public class ShapeFactory {
    public Shape getShape(String shapeType) {
        if (shapeType == null) {
            return null;
        }
        if (shapeType.equals("Circle")) {
            return new Circle();
        } else if (shapeType.equals("square")) {
            return new Square();
        }
        return null;
    }
}
